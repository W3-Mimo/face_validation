<?php

namespace w3engineers\face_validation;

class FaceValidation
{
    public function greet(String $sName)
    {
        return 'Hi ' . $sName . '! How are you doing today?';
    }
}
