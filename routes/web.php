<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use w3engineers\face_validation\FaceValidation;

Route::get('/face_validation/{name}', function ($name) {
    $oGreetr = new FaceValidation();
    return $oGreetr->greet($name);
//    return view('welcome');
});
